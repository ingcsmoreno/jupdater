/*
 * Copyright 2012 Santiago Moreno <ingcsmoreno@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csm.jupdater.configuration;

import csm.uttils.Archive;
import java.util.HashMap;
import org.apache.log4j.Logger;

/**
 * This class contains all the necessary configurations properties for
 * all Drecker modules.
 * 
 * Use "getProperty" and "saveProperty" method for adding, updating or getting
 * a drecker property.
 * 
 * The initial values are taken from dreck.properties file, located on conf
 * folder on the application path.
 * @author Santiago Moreno <ingcsmoreno@gmail.com>
 */
public class ConfigurationManager {

    static {
        properties = new HashMap<String, String>();
        loadProperties("./target/jUpdater/conf/jupdater.properties");
        //loadProperties("./conf/jupdater.properties");
    }
    public static HashMap<String, String> properties;

    /**
     * Returns the value of a value asosiated with the given String key.
     * If there is no value for the given key, it returns null.
     * @param propertyKey
     * @return An string assosiated with the String key given by parameter
     */
    public static String getProperty(String propertyKey) {
        Logger.getLogger(ConfigurationManager.class.getName()).
                debug("Reading property " + propertyKey);
        return properties.get(propertyKey);
    }

    /**
     * Associates the specified property value with the specified key on a hashmap.
     * If the map previously contained a property for the key, the old value
     * is replaced.
     * @param propertyKey
     * @param propertyValue 
     */
    public static void saveProperty(String propertyKey, String propertyValue) {
        Logger.getLogger(ConfigurationManager.class.getName()).
                debug("Saving property " + propertyKey);
        properties.put(propertyKey, propertyValue);
    }

    static void loadProperties(String path) {
        csm.uttils.Archive propFile = new Archive(path);
        String propString = propFile.readString();
        Logger.getLogger(ConfigurationManager.class.getName()).debug("File: "
                + propFile.getFile().exists());

        Logger.getLogger(ConfigurationManager.class.getName()).debug("File Path: "
                + propFile.getFile().getAbsolutePath());

        String[] properties = propString.split("\n");
        for (String property : properties) {
            String[] propertyString = property.split("=");
            if (propertyString.length == 2) {
                Logger.getLogger(ConfigurationManager.class.getName()).debug(property);
                saveProperty(propertyString[0], propertyString[1]);
            }
        }
    }

    public static int getAlignmentValue(String value) {
        switch (value) {
            case "RIGHT":
                return javax.swing.SwingConstants.RIGHT;
            case "LEFT":
                return javax.swing.SwingConstants.LEFT;
            case "TOP":
                return javax.swing.SwingConstants.TOP;
            case "BOTTOM":
                return javax.swing.SwingConstants.BOTTOM;
            default:
                return javax.swing.SwingConstants.CENTER;
        }
    }
}
