/*
 * Copyright (C) 2012 Santiago Moreno <ingcsmoreno@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.csm.jupdater.logic;

/**
 *
 * @author Santiago Moreno <ingcsmoreno@gmail.com>
 */
public class Update {
    
    private int updateCode;
    private String updateFile, checksum, description;
    private boolean critical;

    public Update (){
        updateCode = -1;
        updateFile ="";
        checksum = "";
        description = "";    
        critical = false;
    }
    
    /**
     * @return the updateCode
     */
    public int getUpdateCode() {
        return updateCode;
    }

    /**
     * @param updateCode the updateCode to set
     */
    public void setUpdateCode(int updateCode) {
        this.updateCode = updateCode;
    }

    /**
     * @return the updateFile
     */
    public String getUpdateFile() {
        return updateFile;
    }

    /**
     * @param updateFile the updateFile to set
     */
    public void setUpdateFile(String updateFile) {
        this.updateFile = updateFile;
    }

    /**
     * @return the checksum
     */
    public String getChecksum() {
        return checksum;
    }

    /**
     * @param checksum the checksum to set
     */
    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the critical
     */
    public boolean isCritical() {
        return critical;
    }

    /**
     * @param critical the critical to set
     */
    public void setCritical(boolean critical) {
        this.critical = critical;
    }
}
