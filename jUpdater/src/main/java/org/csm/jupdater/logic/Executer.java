/*
 * Copyright (C) 2012 Santiago Moreno <ingcsmoreno@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.csm.jupdater.logic;

import java.io.IOException;

/**
 *
 * @author Santiago Moreno <ingcsmoreno@gmail.com>
 */
public class Executer {
    
    public static void executeJar(String path) throws IOException, InterruptedException 
    {
        execute(new String[]{"java","-jar",path});
    }
    
    private static void execute(String[] command)throws IOException, InterruptedException
    {
        Process ps=Runtime.getRuntime().exec(command);
        ps.waitFor();
        java.io.InputStream is=ps.getInputStream();
        byte b[]=new byte[is.available()];
        is.read(b,0,b.length);
        System.out.println(new String(b));
    }
    
}
