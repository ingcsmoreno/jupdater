/*
 * Copyright (C) 2012 Santiago Moreno <ingcsmoreno@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.csm.jupdater.logic;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.csm.jupdater.UpdateManager;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Santiago Moreno <ingcsmoreno@gmail.com>
 */
public class XMLInterpreter {

    public XMLInterpreter(String url)
            throws ParserConfigurationException, SAXException, IOException {

        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();

        UpdatesXMLHandler handler = new UpdatesXMLHandler();

        saxParser.parse(url, handler);
    }

    private enum Elements {

        OTHER,
        CODE,
        FILE,
        CHECKSUM,
        DESCRIPTION,
        CRITICAL
    }

    public class UpdatesXMLHandler extends DefaultHandler {

        private Elements element;
        private Update updateData = null;

        public void startElement(String uri, String localName, String qName,
                Attributes attributes) throws SAXException {

            element = Elements.OTHER;

            if (qName.equalsIgnoreCase("lastVersion")) {
                UpdateManager.setLastVersion(Integer.parseInt(attributes.getValue("version")));
                UpdateManager.setLastUpdate(Integer.parseInt(attributes.getValue("update")));
            }

            if (qName.equalsIgnoreCase("update")) {
                updateData = new Update();
            }

            if (qName.equalsIgnoreCase("updateCode")) {
                element = Elements.CODE;
            }

            if (qName.equalsIgnoreCase("updateFile")) {
                element = Elements.FILE;
            }

            if (qName.equalsIgnoreCase("checksum")) {
                element = Elements.CHECKSUM;
            }

            if (qName.equalsIgnoreCase("description")) {
                element = Elements.DESCRIPTION;
            }

            if (qName.equalsIgnoreCase("isCritical")) {
                element = Elements.CRITICAL;
            }
        }

        public void endElement(String uri, String localName,
                String qName) throws SAXException {

            if (qName.equalsIgnoreCase("update") && null != updateData) {
                UpdateManager.getUpdates().put(updateData.getUpdateCode(), updateData);
                updateData = null;
            }
            element = Elements.OTHER;

        }

        public void characters(char ch[], int start, int length) throws SAXException {

            if (null != updateData) {
                switch (element) {
                    case CODE:
                        updateData.setUpdateCode(
                                Integer.parseInt(new String(ch, start, length)));
                        break;
                    case FILE:
                        updateData.setUpdateFile(new String(ch, start, length));
                        break;
                    case CHECKSUM:
                        updateData.setChecksum(""+new String(ch, start, length));
                        break;
                    case DESCRIPTION:
                        updateData.setDescription(new String(ch, start, length));
                        break;
                    case CRITICAL:
                        updateData.setCritical(
                                Boolean.parseBoolean(new String(ch, start, length)));
                        break;
                }
            }
        }
    }
}
