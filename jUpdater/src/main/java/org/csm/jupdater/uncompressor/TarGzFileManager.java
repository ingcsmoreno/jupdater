/*
 * Copyright (C) 2012 Santiago Moreno <ingcsmoreno@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.csm.jupdater.uncompressor;

import java.io.File;
import java.io.FileNotFoundException;
import org.apache.log4j.Logger;
import org.codehaus.plexus.archiver.ArchiverException;
import org.codehaus.plexus.archiver.tar.TarGZipUnArchiver;
import org.csm.jupdater.UpdateManager;
import org.csm.jupdater.configuration.ConfigurationManager;
import org.csm.jupdater.downloader.DownloadManager;
import org.csm.jupdater.logic.Update;

/**
 *
 * @author Santiago Moreno <ingcsmoreno@gmail.com>
 */
public class TarGzFileManager {

    /**
     * 
     * @param file
     * @throws FileNotFoundException 
     */
    private static boolean untarUpdate(String file, String destinationDirectory) throws FileNotFoundException {
        boolean untarResutl = true;
        try {
            //Create a File object
            File targzFile = new File(file);
            File destinationFile = new File(destinationDirectory);

            if (!destinationFile.exists())
            {
                destinationFile.mkdirs();
            }
            
            if (targzFile.exists()) {
                //Create an UnArchiver object from .tar.gz file
                TarGZipUnArchiver targzUnArchiver = new TarGZipUnArchiver();

                //Set the compressed source file
                targzUnArchiver.setSourceFile(targzFile);
                                
                //extract the .tar.gz file on destination Path
                targzUnArchiver.extract("", destinationFile);
            }
            else
            {
                throw new FileNotFoundException("File "+file + " missing.");
            }
        } catch (ArchiverException ex) {
            Logger.getLogger(TarGzFileManager.class).error(ex.getStackTrace());
            untarResutl = false;
        }
        return untarResutl;
    }

    public static void uncompressUpdate(int updateCode) throws FileNotFoundException {
        Update update = UpdateManager.getUpdate(updateCode);

        String fileName = DownloadManager.getFileName(
                update.getUpdateFile());

        String destinationFolder = ConfigurationManager.getProperty("INSTALL_PATH");

        untarUpdate(fileName, destinationFolder);
    }
}
