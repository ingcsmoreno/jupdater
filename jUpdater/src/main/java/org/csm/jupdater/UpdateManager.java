/*
 * Copyright 2012 Santiago Moreno <ingcsmoreno@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csm.jupdater;

import csm.uttils.Archive;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.csm.jupdater.configuration.ConfigurationManager;
import org.csm.jupdater.downloader.DownloadManager;
import org.csm.jupdater.logic.Update;
import org.csm.jupdater.uncompressor.TarGzFileManager;

/**
 *
 * @author Santiago Moreno <ingcsmoreno@gmail.com>
 */
public class UpdateManager {

    private static int lastVersion, lastUpdate;
    private static HashMap<Integer, Update> updates =
            new HashMap<Integer, Update>();
    
    public static String getActualVersion()
    {
        return "Actual: v" + ConfigurationManager.getProperty("APP_VERSION")
                    +"u"+ConfigurationManager.getProperty("APP_UPDATE");
    }
    
    public static void downloadRemainingUpdates() throws IOException
    {
        int actualUpdate = Integer.parseInt(ConfigurationManager.getProperty("APP_UPDATE"));
        
        while (actualUpdate < getLastUpdate() )
        {
            //Increase update number to next one.
            actualUpdate++;
            
            //download next update
            DownloadManager.downloadUpdate(actualUpdate);
        }
    }

    /**
     * 
     */
    static void installDownloadedUpdates() throws FileNotFoundException {
        int actualUpdate = Integer.parseInt(ConfigurationManager.getProperty("APP_UPDATE"));
        
        while (actualUpdate < getLastUpdate() )
        {
            //Increase update number to next one.
            actualUpdate++;
            
            //download next update
            TarGzFileManager.uncompressUpdate(actualUpdate);
        }
    }
    
    /**
     * @return the version
     */
    public static int getLastVersion() {
        return lastVersion;
    }

    /**
     * @param aVersion the version to set
     */
    public static void setLastVersion(int aVersion) {
        lastVersion = aVersion;
    }

    /**
     * @return the update
     */
    public static int getLastUpdate() {
        return lastUpdate;
    }

    /**
     * @param aUpdate the update to set
     */
    public static void setLastUpdate(int aUpdate) {
        lastUpdate = aUpdate;
    }

    /**
     * @return the updates
     */
    public static HashMap<Integer, Update> getUpdates() {
        return updates;
    }
    
    public static Update getUpdate(int updateCode)
    {
        return updates.get(updateCode);
    }

}
