/*
 * Copyright 2012 Santiago Moreno <ingcsmoreno@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csm.jupdater;

import java.io.IOException;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.csm.jupdater.configuration.ConfigurationManager;
import org.csm.jupdater.downloader.DownloadManager;
import org.csm.jupdater.gui.SplashScreen;
import org.csm.jupdater.logic.Executer;

public class App {

    public static void main(String[] args) {

        new Thread() {

            public void run() {
                SplashScreen sp = new SplashScreen(ConfigurationManager.getProperty("SPLASH_IMAGE"));
                try {
                    sp.setVisible(true);
                    sp.changeShowedMessage("Initiating jUpdater");
                    sp.changeShowedMessage("Checking Versions");
                    sp.changeShowedMessage(UpdateManager.getActualVersion());

                    int updatesRemaining = DownloadManager.checkForUpdates(ConfigurationManager.getProperty("UPDATE_FILE_URL"),
                            ConfigurationManager.getProperty("UPDATE_FILE_PATH"));
                    sp.changeShowedMessage("Updates Remaning: " + updatesRemaining);

                    UpdateManager.downloadRemainingUpdates();

                    UpdateManager.installDownloadedUpdates();

                    sp.changeShowedMessage("Executing: "
                            + ConfigurationManager.getProperty("EXECUTION_FILE"));

                    Executer.executeJar(ConfigurationManager.getProperty("EXECUTION_FILE"));

                    sp.changeShowedMessage("Done");

                } catch (InterruptedException ex) {
                    Logger.getLogger(App.class.getName()).error(
                            ex.getStackTrace());
                } catch (IOException ex) {
                    Logger.getLogger(App.class.getName()).error(
                            ex.getStackTrace());
                } catch (Exception ex) {
                    Logger.getLogger(App.class.getName()).warn(
                            ex.getMessage());
                    Logger.getLogger(App.class.getName()).error(
                            ex.getStackTrace().toString());
                } finally {
                    sp.dispose();
                }
            }
        }.start();
    }
}
