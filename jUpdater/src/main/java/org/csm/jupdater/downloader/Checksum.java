/*
 * Copyright (C) 2012 Santiago Moreno <ingcsmoreno@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.csm.jupdater.downloader;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.log4j.Logger;

/**
 *
 * @author Santiago Moreno <ingcsmoreno@gmail.com>
 */
class Checksum {

    public static String getSHA1sumChecksum(byte[] data) {
        return checksum(data, "SHA1");
    }

    public static String getMD5Checksum(byte[] data) {
        return checksum(data, "MD5");
    }

    public static boolean checkSHA1(byte[] data, String sha1checksum) {
        return sha1checksum.compareToIgnoreCase(getSHA1sumChecksum(data)) == 0;
    }

    public static boolean checkMD5(byte[] data, String md5checksum) {
        return md5checksum.compareToIgnoreCase(getMD5Checksum(data)) == 0;
    }

    private static String checksum(byte[] data, String algorithm) {
        String checksum = "";
        try {
            MessageDigest msgDigest = MessageDigest.getInstance(algorithm);
            //convert the byte to hex format
            checksum = formatByteArray2Hex(msgDigest.digest(data));
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Checksum.class.getName()).error(
                    ex.getStackTrace());
        }
        return checksum;
    }

    private static String formatByteArray2Hex(final byte[] bytes) {
//        Formatter formatter = new Formatter();
//        for(byte b : bytes)
//        {
//            formatter.format("%02x", b);
//        }
//        return formatter.toString();
        //convert the byte to hex format
        StringBuffer sb = new StringBuffer("");

        for (int i = 0; i < bytes.length; i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
}
