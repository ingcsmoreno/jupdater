/*
 * Copyright (C) 2012 Santiago Moreno <ingcsmoreno@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.csm.jupdater.downloader;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.log4j.Logger;
import org.csm.jupdater.configuration.ConfigurationManager;

/**
 *
 * @author Santiago Moreno <ingcsmoreno@gmail.com>
 */
class Downloader {

    private String url, fileName;

    public Downloader(String urlPath) {
        url = urlPath;
        fileName = DownloadManager.getFileName(urlPath);
    }

    public String downloadFile() throws MalformedURLException, IOException {
        FileOutputStream fos = new FileOutputStream(fileName);
                
        byte byteData[] = getAllBytesFromFile(url);
        
        BufferedOutputStream bos = new BufferedOutputStream(fos,
                byteData.length);

        bos.write(byteData, 0, byteData.length);
        
        bos.close();

        Logger.getLogger(Downloader.class.getName()).info(
                byteData.length + " byte(s) downloaded");
        Logger.getLogger(Downloader.class.getName()).debug(
                "checking downloads integrity.");
        return Checksum.getMD5Checksum(byteData);
    }

    private byte[] getAllBytesFromFile(String URL) throws FileNotFoundException, IOException {
        //Prepare the input file
        BufferedInputStream bis = new BufferedInputStream(new URL(URL).openStream());

        ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();

        //Prepare the bytes array

        byte[] bytes = new byte[1024];

        int nread = 0;
        Logger.getLogger(Downloader.class.getName()).debug(
                "Downloading file from "
                + URL);
        while ((nread = bis.read(bytes)) != -1) {
            //While reading the file, byte buffer
            byteOutStream.write(bytes, 0, nread);
        }
        byte[] bytesData = byteOutStream.toByteArray();
        byteOutStream.close();
        bis.close();
        return bytesData;
    }
}
