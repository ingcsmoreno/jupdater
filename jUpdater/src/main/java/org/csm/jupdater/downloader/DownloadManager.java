/*
 * Copyright (C) 2012 Santiago Moreno <ingcsmoreno@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.csm.jupdater.downloader;

import java.io.IOException;
import java.net.MalformedURLException;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Logger;
import org.csm.jupdater.UpdateManager;
import org.csm.jupdater.configuration.ConfigurationManager;
import org.csm.jupdater.exceptions.ChecksumFailException;
import org.csm.jupdater.logic.Update;
import org.csm.jupdater.logic.XMLInterpreter;
import org.xml.sax.SAXException;

/**
 *
 * @author Santiago Moreno <ingcsmoreno@gmail.com>
 */
public class DownloadManager {

    public static void downloadUpdate(int updateCode) throws ChecksumFailException, IOException {
        Update update = UpdateManager.getUpdate(updateCode);;
        try {
            String updateChecksum = downloadFile(update.getUpdateFile()); 
            Logger.getLogger(DownloadManager.class.getName()).debug("Checksum="
                    +updateChecksum);
            if (updateChecksum.compareTo(update.getChecksum())!=0)
                throw new ChecksumFailException("Downloaded file "
                        + update.getUpdateFile() + " checksum fails.");
        } catch (MalformedURLException ex) {
            Logger.getLogger(DownloadManager.class.getName()).error(
                    "URL: "+update.getUpdateFile()+"\n"+ex.getStackTrace());
        }
    }
    
    /**
     * 
     * @param url
     * @return
     * @throws IOException 
     */
    public static int checkForUpdates(String url, String path) throws IOException {
        int updatesCant = 0;
        try {
            Logger.getLogger(DownloadManager.class.getName()).info(
                    "Downloading file from " + url);
            
            downloadFile(url);

            Logger.getLogger(DownloadManager.class.getName()).debug(
                    "Updates Information File Downloaded Successfully");
            
            Logger.getLogger(DownloadManager.class.getName()).debug(
                    "Parsing Updates Information File");
            XMLInterpreter xmlUpdates = new XMLInterpreter(path + "/updates.xml");

            Logger.getLogger(DownloadManager.class.getName()).debug(
                    "Checking remaining updates.");
            updatesCant = UpdateManager.getLastUpdate()
                    - Integer.parseInt(ConfigurationManager.getProperty("APP_UPDATE"));

        } catch (MalformedURLException ex) {
            Logger.getLogger(DownloadManager.class.getName()).warn(
                    "Malformed URL: " + url);
            Logger.getLogger(DownloadManager.class.getName()).warn(
                    ex.getStackTrace());
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(DownloadManager.class.getName()).warn(
                    ex.getStackTrace());
        } catch (SAXException ex) {
            Logger.getLogger(DownloadManager.class.getName()).warn(
                    ex.getStackTrace());
        }
        return updatesCant;
    }

    /**
     * 
     * @param url
     * @throws MalformedURLException
     * @throws IOException 
     */
    static String downloadFile(String url)
            throws MalformedURLException, IOException {
        Downloader downloader = new Downloader(url);
        return downloader.downloadFile();
    }
    
    public static String getFileName(String urlPath) {
        String[] path = urlPath.split("/");
        return ConfigurationManager.getProperty("UPDATE_FILE_PATH")
                + "/" + path[path.length - 1];
    }
}
